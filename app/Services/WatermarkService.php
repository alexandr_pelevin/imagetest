<?php
namespace App\Services;

use Intervention\Image\Facades\Image;

class WatermarkService
{
    public function getImage($image)
    {
        if(file_exists($image))
        {
            $urlImagePath = explode('.', $image);
            $urlImageName = explode('/', $urlImagePath[0]);
            $name = array_pop($urlImageName);
            $newName = $name.time();
            $mime = $urlImagePath[1];
            $imageMake = Image::make($image);
            $imageMakeTemp = Image::make($image);

            // pick a color at position and format it as hex string
            $height = $imageMakeTemp->height();
            $width = $imageMakeTemp->width();
            $imageCrop = $imageMakeTemp->crop($height/2, $width/2);
            $imageCrop->save('images/temp/'.$newName.'.'.$mime);
            $hex = ColorService::getColor('images/temp/'.$newName.'.'.$mime);
            list($r, $g, $b) = sscanf('#'.$hex, "#%02x%02x%02x");
            unlink('images/temp/'.$newName.'.'.$mime);

            if($r >= $g && $r >= $b)
            {
                $newWatermarka = Image::make('images/test_black.jpg')->resize($width/5, $height/5);
                $img = $imageMake->insert($newWatermarka, 'center');
            }elseif($g >= $r && $g >= $b){
                $newWatermarka = Image::make('images/test_red.jpg')->resize($width/5, $height/5);
                $img = $imageMake->insert($newWatermarka, 'center');
            }else{
                $newWatermarka = Image::make('images/test_yellow.jpg')->resize($width/5, $height/5);
                $img = $imageMake->insert($newWatermarka, 'center');
            }
            $img->save('images/crop/'.$name.'.'.$mime);

            return 'images/crop/'.$name.'.'.$mime;
        }

        return $image;
    }
}
